## The Challenge

Using the following wireframes, build a quiz interface. The questions are all in [questions.json](https://raw.githubusercontent.com/outlier-org/challenge-quiz/master/src/questions.json).

![Question](https://raw.githubusercontent.com/outlier-org/challenge-quiz/master/docs/wire-question.png)

![Correct Answer](https://raw.githubusercontent.com/outlier-org/challenge-quiz/master/docs/wire-answer-correct.png)

![Incorrect Answer](https://raw.githubusercontent.com/outlier-org/challenge-quiz/master/docs/wire-answer-incorrect.png)

![Progress](https://raw.githubusercontent.com/outlier-org/challenge-quiz/master/docs/wire-progress.png)

![Difficulty](https://raw.githubusercontent.com/outlier-org/challenge-quiz/master/docs/wire-difficulty.png)

![Score](https://raw.githubusercontent.com/outlier-org/challenge-quiz/master/docs/wire-score.png)

