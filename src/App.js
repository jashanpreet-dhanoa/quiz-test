import React, { useState } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBBadge, MDBBtn } from "mdbreact";
import "./App.css";
import questions from "../src/questions.json";
import LoadingBar from "react-top-loading-bar";
import StarRatingComponent from "react-star-rating-component";
import { Progress } from "reactstrap";
import { _ } from "underscore";
let resultArr = [];
function App() {
  const [count, setcount] = useState(0);
  const [next, resetNext] = useState(false);
  const [progress, setProgress] = useState(0);
  const [isCorrect, resetAnswer] = useState(null);
  const [CurrentProgress, setCurrentProgress] = useState(0);
  const [probability, setProbability] = useState(100);

  const removeSpecialChar = data => {
    let purifyData = decodeURIComponent(data);
    return purifyData;
  };

  const getDificultyLevel = level => {
    if (level === "easy") {
      return 1;
    } else if (level === "medium") {
      return 2;
    } else if (level === "hard") {
      return 3;
    }
  };

  const generateOptions = (correct, incorrectArr, getIndex) => {
    let createOptionArr = [...incorrectArr, correct];
    let getRandomArr = _.shuffle(createOptionArr);
    return getRandomArr.map((option, index) => {
      return (
        <MDBCol size="6" key={index}>
          <MDBBadge
            tag="a"
            onClick={e => checkIsCorrect(option, correct, getIndex)}
            color="light"
            className="ml-2"
            style={{
              width: "100%",
              marginBottom: "30px",
              padding: "7px 5px",
              border: "1px solid #000",
              borderRadius: "5px",
              textOverflow: "ellipsis",
              overflow: "hidden"
            }}
          >
            {removeSpecialChar(option)}
          </MDBBadge>
        </MDBCol>
      );
    });
  };

  const checkIsCorrect = async (answer, correctAnswer, index) => {
    let realAnswer = null;
    if (answer === correctAnswer) {
      realAnswer = "correct";
      resetAnswer("Correct!");
    } else {
      resetAnswer("Incorrect!");
      realAnswer = "incorrect";
    }

    let getIndex = resultArr.findIndex(item => item.index === index);
    if (getIndex !== -1) {
      resultArr[getIndex].isCorrect = realAnswer;
    } else {
      resultArr.push({
        index: index,
        isCorrect: realAnswer
      });
    }
    calculateCurrentPercentage(resultArr);
    resetNext(true);
  };

  const calculateCurrentPercentage = resultArr => {
    let getTotalAttempt = resultArr.length;
    let correctCount = 0;
    for (let i = 0; i <= getTotalAttempt - 1; i++) {
      if (resultArr[i]["isCorrect"] === "correct") {
        correctCount = correctCount + 1;
      }
    }
    let getCurrentPercentage = (correctCount * 100) / getTotalAttempt;
    if (getTotalAttempt !== correctCount) {
      let getWrongAttempts = getTotalAttempt - correctCount;
      let getPendingCount =
        questions.length - (correctCount + getWrongAttempts);
      let getProbabilityScore =
        ((correctCount + getPendingCount) * 100) / questions.length;
      setProbability(Math.round(getProbabilityScore));
    } else {
      setProbability(100);
    }

    setCurrentProgress(Math.round(getCurrentPercentage));
  };

  const nextQuestion = () => {
    resetNext(false);
    setcount(count + 1);
    let newProgress = progress + 5;
    setProgress(newProgress);
  };

  return (
    <MDBContainer>
      <LoadingBar height={10} color="#f11946" progress={progress} />
      <MDBRow>
        <MDBCol></MDBCol>
        <MDBCol>
          {questions.map((question, index) => {
            return (
              <React.Fragment key={index}>
                {count === index ? (
                  <React.Fragment>
                    <h2>
                      Question {index + 1} of {questions.length}
                    </h2>
                    <span>{removeSpecialChar(question.category)}</span>
                    <br />
                    <StarRatingComponent
                      name="rating"
                      value={getDificultyLevel(question.difficulty)}
                      starCount={3}
                    />
                    <br />
                    <p>{removeSpecialChar(question.question)}</p>
                    <MDBRow>
                      {generateOptions(
                        question.correct_answer,
                        question.incorrect_answers,
                        index
                      )}
                    </MDBRow>
                    {next ? (
                      <React.Fragment>
                        <h3 className="mt-2 mb-2">{isCorrect}</h3>
                        <MDBBtn color="elegant" onClick={e => nextQuestion()}>
                          Next Question
                        </MDBBtn>
                      </React.Fragment>
                    ) : null}
                  </React.Fragment>
                ) : null}
              </React.Fragment>
            );
          })}

          <React.Fragment>
            {questions.length - 1 < count ? "Quiz Completed" : null}
          </React.Fragment>

          <div className="mt-5 text-left">
            Score : {CurrentProgress}% and Max-Score : {probability}%
          </div>
          <Progress value={CurrentProgress} />
        </MDBCol>
        <MDBCol></MDBCol>
      </MDBRow>
    </MDBContainer>
  );
}

export default App;
